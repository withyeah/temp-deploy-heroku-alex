from django.urls import include, re_path
from django.contrib import admin
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
   openapi.Info(
      title="Snippets API",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)


urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^api/auth/', include('accounts.api.urls')),
    re_path(
        r'^api/user/',
        include(
            'accounts.api.user.urls',
            namespace='user',
        ),
    ),
    re_path(
        r'^api/building/',
        include(
            'properties.api.urls',
            namespace='properties',
        )
    ),
    re_path(
        r'^swagger(?P<format>\.json|\.yaml)$', 
        schema_view.without_ui(cache_timeout=0), 
        name='schema-json'
        ),
    re_path(
        r'^swagger/$', 
        schema_view.with_ui('swagger', cache_timeout=0), 
        name='schema-swagger-ui'
        ),
    re_path(
        r'^redoc/$', 
        schema_view.with_ui('redoc', cache_timeout=0), 
        name='schema-redoc'
        ),
]


