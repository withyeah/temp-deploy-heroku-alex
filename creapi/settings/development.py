import os

from .base import *


DEBUG = True

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'cre',
        'USER': 'postgres',
        # 'PASSWORD': os.environ['DATABASE_PASSWORD'],
        'PASSWORD': 'vpCelC0OJ3BDgOnv3jHN',
        'HOST': 'development-db.ce0ovrnguwsi.ap-northeast-2.rds.amazonaws.com',
        'PORT': '5432',
    }
}
