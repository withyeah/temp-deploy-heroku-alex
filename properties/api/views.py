import json

from django.http import JsonResponse, HttpResponse
from django.db.models import Q
from django.core.serializers.json import DjangoJSONEncoder
from django.core.serializers import serialize
# from django.contrib.gis.geos import GEOSGeometry
from django.shortcuts import get_object_or_404
from rest_framework import generics, mixins, permissions, viewsets
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response
from rest_framework.views import APIView
# from django.contrib.gis.db.models.functions import Centroid

from accounts.api.permissions import IsOwnerOrReadOnly
from properties.models import (
    Building, Transaction, 
    Jipgaegu, Condition, AdjustValue, Neighborhood,
)
from .serializers import (
    BuildingSerializer, 
    TransactionSerializer, JipgaeguSerializer, 
    JipgaeguSerializer, 
    ConditionSerializer, AdjustValueSerializer,
    NeighborhoodSerializer,
)


# class PropertyAPIView(generics.RetrieveAPIView):
#     serializer_class = PropertySerializer
#     queryset = Property.objects.all()
#     lookup_field = 'uuid'


# class JipgaeguPolygonAPIView(APIView):

#     def get_object(self, coords):
#         serializer_class = JipgaeguSerializer
#         queryset = Jipgaegu.objects.filter(geometry__contains=coords)
#         return Response(queryset)


class BuildingAPIView(APIView):
    
    def get(self, request, address):
        serializer_class = BuildingSerializer
        queryset = Building.objects.filter(Q(address_road__icontains=address) | Q(address_dong__icontains=address))
        serialized = serialize('json', queryset)
        return HttpResponse(serialized, content_type="text/json-comment-filtered")


class BuildingAPIDetailView(APIView):

    def get(self, request, address):
        serializer_class = BuildingSerializer
        queryset = Building.objects.filter(Q(address_road__icontains=address) | Q(address_dong__icontains=address))
        serialized = serialize('json', queryset)
        return HttpResponse(serialized, content_type="text/json-comment-filtered")


class TransactionAPIView(generics.RetrieveAPIView):
    serializer_class = TransactionSerializer
    queryset = Transaction.objects.all()
    lookup_field = 'building_id'


class ConditionAPIView(generics.CreateAPIView):
    serializer_class = ConditionSerializer
    queryset = Condition.objects.all()


class AdjustValueAPIView(generics.CreateAPIView):
    serializer_class = AdjustValueSerializer
    queryset = AdjustValue.objects.all()


# class NeighborhoodGeoAPIView(APIView):

#     def get(self, request, coords):
#         result = {} 
#         coords = GEOSGeometry(coords)
#         neighborhood = Neighborhood.objects.filter(geometry__contains=coords).first()
#         serialized = serialize(
#             'json', Jipgaegu.objects.filter(
#                 geometry__within=neighborhood.geometry).only(
#                     'population', 'population_density', 'average_age'))
#         return HttpResponse(serialized)


# class NeighborhoodStatsAPIView(APIView):

#     def get(self, request, coords):
#         coords = GEOSGeometry(coords)
#         neighborhood = Neighborhood.objects.filter(geometry__contains=coords).first()
#         buildings = Building.objects.filter(geometry__within=neighborhood.geometry)
#         stats = []
#         for building in buildings:
#             for transaction in building.transaction_set.all():
#                 serialized = json.loads(serialize('json', [transaction,]))[0].get('fields')
#                 stats.append(serialized)
#         return JsonResponse(stats, safe=False)