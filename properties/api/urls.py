from django.urls import re_path, path

from .views import (
    BuildingAPIView, 
    #NeighborhoodGeoAPIView,
    BuildingAPIDetailView, #excluded properties view
    #JipgaeguPolygonAPIView, 
    TransactionAPIView,
    ConditionAPIView, 
    AdjustValueAPIView, 
    #NeighborhoodStatsAPIView,
)


app_name = 'properties'
urlpatterns = [
    # re_path(
    #     r'^property/$', 
    #     PropertyAPIView.as_view(), 
    #     name='property'
    # ),
    # re_path(
    #     r'^jipgaegu/$',
    #     JipgaeguPolygonAPIView.as_view(),
    #     name='jipgaegu',
    # ),
    re_path(
        r'^building/(?P<address>[\s\S]+)/$', 
        BuildingAPIView.as_view(),
        name='building',
    ),
    re_path(
        r'^building-detail/(?P<address>[\s\S]+)/$',
        BuildingAPIDetailView.as_view(),
        name='building_detail',
    ),
    re_path(
        r'^transaction/$',
        TransactionAPIView.as_view(),
        name='transaction',
    ),
    re_path(
        r'^condition/$',
        ConditionAPIView.as_view(),
        name='condition',
    ),
    re_path(
        r'^adjust-value/$',
        AdjustValueAPIView.as_view(),
        name='adjust_value',
    ), 
    # re_path(
    #     r'^neighborhood-geo/(?P<coords>[\s\S]+)/$',
    #     NeighborhoodGeoAPIView.as_view(),
    #     name='neighborhood_geo',
    # ), 
    # re_path(
    #     r'^neighborhood-stats/(?P<coords>[\s\S]+)/$',
    #     NeighborhoodStatsAPIView.as_view(),
    #     name='neighborhood_stats',
    # )
]
