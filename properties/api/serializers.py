from rest_framework import serializers
# from django.core.serializers import serialize

from accounts.api.serializers import UserPublicSerializer
from properties.models import (
    Building, Transaction, 
    Jipgaegu, Condition, AdjustValue, Neighborhood,
)


class NeighborhoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = Neighborhood
        fields = '__all__'
    

# class PropertySerializer(serializers.ModelSerializer):
#     url = serializers.SerializerMethodField(read_only=True)
#     building_url = serializers.SerializerMethodField(read_only=True)
#     class Meta:
#         model = Property
#         fields = '__all__'
    
#     def get_domain(self):
#         return 'https://' + self.context['request'].META['HTTP_HOST']

#     def get_url(self, obj):
#         domain = self.get_domain()
#         return domain + "/api/building/property/{uuid}/".format(uuid=obj.uuid)

#     def get_building_url(self, obj):
#         domain = self.get_domain()
#         return domain + "/api/building/{uuid}/".format(uuid=obj.building.uuid)


class BuildingSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField(read_only=True)
    hist_price = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Building
        fields = '__all__'

    def get_domain(self):
        return 'https://' + self.context['request'].META['HTTP_HOST']

    def get_url(self, obj):
        domain = self.get_domain()
        return domain + "/api/building/{uuid}/".format(uuid=obj.uuid)


class JipgaeguSerializer(serializers.ModelSerializer):
    neighborhood = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Jipgaegu
        fields = '__all__'

    def get_neighborhood(self, obj):
        return obj.neighborhood.name


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = '__all__'


class ConditionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Condition
        fields = '__all__'


class AdjustValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdjustValue
        fields = '__all__'