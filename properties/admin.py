from django.contrib import admin

from .models import City, District, Neighborhood, Building #, Property


admin.site.register(City)
admin.site.register(District)
admin.site.register(Neighborhood)
admin.site.register(Building)
# admin.site.register(Property)