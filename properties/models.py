import uuid as uuid_lib
from django.db import models
from django.contrib.postgres.fields import JSONField
# from django.contrib.gis.db import models as geo_models


class BaseGeoModel(models.Model):
    uuid = models.UUIDField(
        primary_key=True,
        default=uuid_lib.uuid4,
        editable=False
    )

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    sgis_code = models.CharField(max_length=128, null=True, blank=True)
    name = models.CharField(max_length=64, null=True, blank=True)
    name_en = models.CharField(max_length=64, null=True, blank=True)

    # geometry = geo_models.GeometryField(null=True, blank=True)

    def __str__(self):
        return str(self.name_en)

    class Meta:
        abstract = True


class City(BaseGeoModel):

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name_plural = 'Cities'


class District(BaseGeoModel):

    def __str__(self):
        return str(self.name)


class Neighborhood(BaseGeoModel):

    def __str__(self):
        return str(self.name)


class Jipgaegu(BaseGeoModel):    
    population = models.FloatField(null=True, blank=True)
    population_density = models.FloatField(null=True, blank=True)
    average_age = models.FloatField(null=True, blank=True)

    def __str__(self):
        return str(self.sgis_code)


class Building(BaseGeoModel):
    unique_id = models.CharField(max_length=32, null=True, blank=True, unique=True)

    est_price_per_sqm = models.FloatField(null=True, blank=True)
    est_price = models.FloatField(null=True, blank=True)
    est_date = models.DateTimeField(null=True, blank=True)

    adm_code = models.CharField(max_length=64, null=True, blank=True)
    address_dong = models.CharField(max_length=128, null=True, blank=True)
    address_road = models.CharField(max_length=128, null=True, blank=True)

    main_use_name = models.CharField(max_length=64, null=True, blank=True)
    main_use_code = models.CharField(max_length=64, null=True, blank=True)
    structure_name = models.CharField(max_length=64, null=True, blank=True)
    structure_code = models.CharField(max_length=64, null=True, blank=True)
    roof_type_name = models.CharField(max_length=64, null=True, blank=True)
    roof_type_code = models.CharField(max_length=64, null=True, blank=True)

    construction_year = models.CharField(max_length=16, null=True, blank=True)

    height = models.FloatField(null=True, blank=True)
    aboveground = models.FloatField(null=True, blank=True)
    underground = models.FloatField(null=True, blank=True)

    land_price_per_sqm = models.FloatField(null=True, blank=True)
    area_usage = models.CharField(max_length=64, null=True, blank=True)
    # 연면적
    total_area = models.FloatField(null=True, blank=True)
    # 건물건축면적
    base_area = models.FloatField(null=True, blank=True)
    # 건물대지면적
    land_area = models.FloatField(null=True, blank=True)
    # 건폐율
    base_to_land = models.FloatField(null=True, blank=True)
    # 용적율
    total_to_land = models.FloatField(null=True, blank=True)

    passenger_elev = models.FloatField(null=True, blank=True)
    emergency_elev = models.FloatField(null=True, blank=True)

    CODE_APT = 'apt'
    CODE_COMMERCIAL = 'com'
    CODE_DANDOK = 'dan'
    CODE_OFFICETEL = 'off'
    CODE_YEONLIP = 'yeo'

    CODE_CHOICES = (
        (CODE_APT, 'apt'),
        (CODE_COMMERCIAL, 'commercial'),
        (CODE_DANDOK, 'dandok'),
        (CODE_OFFICETEL, 'officetel'),
        (CODE_YEONLIP, 'yeonlip'),
    )

    building_type = models.CharField(max_length=3, choices=CODE_CHOICES)

    def __str__(self):
        return str(self.unique_id)


class Transaction(models.Model):
    building = models.ForeignKey(Building, on_delete=models.CASCADE, to_field='unique_id')

    date = models.DateField(null=True, blank=True)
    price = models.FloatField(null=True, blank=True)


# class Property(BaseGeoModel):
#     est_price = models.BigIntegerField(null=True, blank=True)
#     est_date = models.DateTimeField(null=True, blank=True)

#     main_use_name = models.CharField(max_length=64, null=True, blank=True)
#     main_use_code = models.CharField(max_length=64, null=True, blank=True)
#     floor = models.IntegerField(null=True, blank=True)

#     def __str__(self):
#         return str(self.code)

#     class Meta:
#         verbose_name_plural = 'Properties'


class Condition(models.Model):
    building = models.ForeignKey(Building, on_delete=models.CASCADE)

    condition = models.IntegerField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)


class AdjustValue(models.Model):
    building = models.ForeignKey(Building, on_delete=models.CASCADE)

    value = JSONField(null=True, blank=True)
