from django.urls import include, re_path
from django.contrib import admin
from rest_framework_jwt.views import refresh_jwt_token, obtain_jwt_token

from .views import AuthAPIView, RegisterAPIView


app_name = 'accounts'
urlpatterns = [
    re_path(r'^$', AuthAPIView.as_view()),
    re_path(r'^register/$', RegisterAPIView.as_view()),
    re_path(r'^jwt/$', obtain_jwt_token),
    re_path(r'^jwt/refresh/$', refresh_jwt_token),
]
