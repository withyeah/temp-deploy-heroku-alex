from django.urls import include, re_path
from django.contrib import admin

from .views import UserDetailAPIView

app_name = 'accounts-user'
urlpatterns = [
    re_path(r'^(?P<username>\w+)/$', UserDetailAPIView.as_view(), name='detail'),
]